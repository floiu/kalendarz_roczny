var YearCalendar = function(selector, options){

    moment.locale('pl');
    var _year = options.year;
    var _events = options.events;
    var dayClick = options.dayClick;
    var romanNumbers = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];

    var drawYearCalendar = function(selector, year) {
        var year = moment(year).startOf('year');
        var yearIt = moment(year).startOf('year');

        $(selector).html("");  

        drawYearCalendarHeader(selector);

        while(yearIt.isSame(year, "year")) {
            $(selector).append("<div class='bfs_year-calendar-row'><div class='bfs_year-calendar-header-cell bfs_year-calendar-month-cell'>"+ romanNumbers[yearIt.format("M")-1] +"</div></div>");
            drawMonth(yearIt);
            fillMonth(yearIt.format('M'), drawEvents(yearIt))
            yearIt.add(1, "month")
        }
    }

    var drawYearCalendarHeader = function(selector) {
        $(selector).append("<div class='bfs_year-calendar-header'><div class='bfs_year-calendar-header-cell'>&nbsp;</div></div>");
        for(var i=1; i<=37; i++) {
            if(i%7 == 1){
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>Po</div>");
            } else if(i%7 == 2) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>Wt</div>");
            } else if(i%7 == 3) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>Śr</div>");
            } else if(i%7 == 4) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>Cz</div>");
            } else if(i%7 == 5) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>Pt</div>");
            } else if(i%7 == 6) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell'>So</div>");
            } else if(i%7 == 0) {
                $("div.bfs_year-calendar-header").append("<div class='bfs_year-calendar-header-cell' style='color: #ff0000;'>Nd</div>");
            }
        }
    }

    var drawMonth = function(date) {
        var yearIt = moment(date).startOf('week');
        var monthRow = $("div.bfs_year-calendar-row").eq(date.format("M")-1);
        
        while(yearIt.isSameOrBefore(date, "month")) {
            monthRow.append("<div data-date='" + yearIt.format("YYYY/MM/DD")+ "' class='bfs_year-calendar-cell'></div>");
            yearIt.add(1, "day")
        }
        while(monthRow.children().length <= 37) {
            monthRow.append("<div class='bfs_year-calendar-cell-no_day'></div>");
        }
        monthRow.children("div.bfs_year-calendar-cell").last().css("border-right", "1px solid #e5e5e5");
    }

    // to od przesuwania (poniedzialki pod poniedzialkami itd)
    var fillMonth = function(month) {
        $("div.bfs_year-calendar-row").eq(month-1).children("div.bfs_year-calendar-cell").each(function(cellIndex){
                    if(moment($(this).attr("data-date")).format("M") != month)  {
                        $(this).addClass("bfs_year-calendar-cell-no_day");
                        $(this).removeClass("bfs_year-calendar-cell");

                    }      

                    if(moment($(this).attr("data-date")).format("M") == month){
                        if(moment($(this).attr("data-date")).format("d") == 0){
                            $(this).html("<div class='bfs_year-calendar-cell__grid-day' style='color: #ff0000;'>" + moment($(this).attr("data-date")).format("D") + "</div>");
                        } else {
                            $(this).html("<div class='bfs_year-calendar-cell__grid-day'>" + moment($(this).attr("data-date")).format("D") + "</div>");
                        }

                        if (moment(moment($(this).attr("data-date")).format("YYYY/MM/DD")).isSame(moment().format("YYYY/MM/DD"))) {
                            $(this).css("background-color", "rgba(240, 173, 78, 0.5)")
                        }
                
                        $(this).on('mouseenter', function(){
                            $(this).parent("div.bfs_year-calendar-row").children("div.bfs_year-calendar-month-cell").addClass("bfs_year-calendar-cell_hovered_day");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+1).addClass("bfs_year-calendar-cell_hovered_day");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+2).css("border-left", "1px solid #e5e5e5");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+1).css("border-left", "1px solid #e5e5e5");
                            for(var i=0; i<=month-1; i++){
                                $("div.bfs_year-calendar-row").eq(i).children().eq(cellIndex+1).addClass("bfs_year-calendar-cell_gray");
                            }
                            for(var i=0; i<=cellIndex; i++){
                                $("div.bfs_year-calendar-row").eq(month-1).children().eq(i).addClass("bfs_year-calendar-cell_gray-month");
                            }

                        });
                        $(this).on('click', function() {
                            dayClick($(this).attr("data-date"));
                        });
                        $(this).on('mouseleave', function(){
                            $(this).parent("div.bfs_year-calendar-row").children("div.bfs_year-calendar-month-cell").removeClass("bfs_year-calendar-cell_hovered_day");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+1).removeClass("bfs_year-calendar-cell_hovered_day");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+2).css("border-left", "none");
                            $("div.bfs_year-calendar-header").children().eq(cellIndex+1).css("border-left", "none");
                            for(var i=0; i<=month-1; i++){
                                $("div.bfs_year-calendar-row").eq(i).children().eq(cellIndex+1).removeClass("bfs_year-calendar-cell_gray");
                            }
                            for(var i=0; i<=cellIndex; i++){
                                $("div.bfs_year-calendar-row").eq(month-1).children().eq(i).removeClass("bfs_year-calendar-cell_gray-month");
                            }
                        });
                        $(this).append(drawEvents($(this).attr("data-date")));
                    }


                    
                
            });


    }

    var drawEvents = function(date) {
        var eventsTag = ""

        var eventsList = _events.filter(function(x) {
                return moment(date).isSameOrAfter(moment(x.start)) && moment(date).isSameOrBefore(moment(x.end));
        });

        var actuallyRow = 3;
        var actuallyColumn = 1;

        for(var i=0; i<eventsList.length; i++) {
            if(actuallyColumn > 3) {
                actuallyColumn = 1;
                actuallyRow--;
            }
            eventsTag += "<div class='bfs_year-calendar-cell__grid-event bfs_year-calendar-cell_grid-"+String(actuallyRow) + String(actuallyColumn)+"' style=background-color:"+ eventsList[i].color +"></div>"
            actuallyColumn++;
        }

        return eventsTag;
    }
    drawYearCalendar(selector, _year);
    this.next = function(){
        _year = parseInt(_year)+1;
        drawYearCalendar(selector, String(_year));
    }

    this.prev = function(){
        _year = parseInt(_year)-1;
        drawYearCalendar(selector, String(_year));
    }

    this.actually = function(){
        _year = moment().format("YYYY");
        drawYearCalendar(selector, String(_year));
    }

    this.setEvents = function(events) {
        _events = events;
        drawYearCalendar(selector, String(_year));
    }
}

var calendar = new YearCalendar("div.bfs_year-calendar", {year: "2020", events: [{start: '2020-02-04', end: '2020-02-07', color: '#4148b1'},
{start: '2020-02-04', end: '2020-02-15', color: '#41acb1'},
{start: '2020-02-04', end: '2020-02-15', color: '#aa32b1'},
{start: '2020-02-04', end: '2020-02-17', color: '#acacb1'},
{start: '2020-03-01', end: '2020-03-07', color: '#41b159'},
{start: '2020-05-21', end: '2020-05-30', color: '#413259'},
{start: '2020-07-10', end: '2020-07-12', color: '#be12ac'},
{start: '2020-10-02', end: '2020-10-05', color: '#12ad00'},
{start: '2020-10-02', end: '2020-10-14', color: '#ffad23'},
{start: '2020-12-24', end: '2020-12-29', color: '#00adbb'},
{start: '2020-08-19', end: '2020-08-26', color: '#99ff11'},
{start: '2021-08-19', end: '2021-08-26', color: '#99ff11'}], dayClick: function(sth){$("div.bfs_dayClick").text("Kliknięty dzień: " + sth)}});